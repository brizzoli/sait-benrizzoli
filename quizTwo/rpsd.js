var gamePieces = new Array ("rock", "paper", "scissors", "dynamite"); 

var textValueFromTxtInput = document.getElementById("playerChoice");

function validate(playerChoice){
    
    if(gamePieces.indexOf(playerChoice) === -1){
        return false;
    }else{
        return true;
    }
}


    // Random number generator, pulling from the array above
    function getRandomGamePiece(arraySize){
        var rnd = Math.floor(Math.random() * arraySize);
        // Output Random Game Piece
        return rnd;
    }

    function opponentPiece(num){ 
        switch(num){
            case 0: 
                return "rock";
                break;
            case 1: 
                return "paper";
                break; 
            case 2: 
                return "scissors";
                break;
            case 3:
                return "dynamite";
                break;                               
        }
    };

    // Function for whoWins
    function whoWins(playerChoice, comChoice) {
        if (playerChoice == "rock") {
            switch(comChoice){
                case 0: //rock
                    return "It's a tie";
                    break;
                case 1: //paper
                    return "Paper wins";
                    break; 
                case 2: //scissors
                    return "Rock wins";
                    break;
                case 3: //dynamite
                    return "Dynamite wins";
                    break;
            }                               
        }
        else if (playerChoice == "scissors") {
            switch(comChoice){
                case 0: //rock
                    return "Rock wins";
                    break;
                case 1: //paper
                    return "Paper win";
                    break; 
                case 2: //scissors
                    return "It's a tie";
                    break;
                case 3: //dynamite
                    return "Dynamite win";
                    break;
            }                               
        }
        else if (playerChoice == "paper") {
            switch(comChoice){
                case 0: //rock
                    return "You win";
                    break;
                case 1: //paper
                    return "It's a tie";
                    break; 
                case 2: //scissors
                    return "Scissors wins";
                    break;
                case 3: //dynamite
                    return "Dynamite wins";
                    break;
            }                               
        }
        else if (playerChoice == "dynamite") {
            switch(comChoice){
                case 0: //rock
                    return "You win";
                    break;
                case 1: //paper
                    return "You win";
                    break; 
                case 2: //scissors
                    return "Scissors wins";
                    break;
                case 3: //dynamite
                    return "It's a tie";
                    break;
            }                               
        }
    }

    // jQuery for returning the results as determined from the if/switch
    $("#playerChoice").click(function(){
        var playerPiece = prompt('Make your pick! (rock, paper, scissors, or dynamite)').toLowerCase();
        if(validate(playerPiece)){

            var opponentChoice = getRandomGamePiece(gamePieces.length);
            $("#results").text("You chose " + playerPiece + " and your opponent chose " + opponentPiece(opponentChoice) + ". " + whoWins(playerPiece, opponentChoice) + "!");
        }else{
            $("#results").text(playerPiece + " is not a valid choice!");
        }
    });   
