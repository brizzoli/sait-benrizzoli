function loadProvinces(){
    var provArray = ["-Select-","Alberta", "British Columbia", "Saskatchewan","Manitoba","Ontario", "Quebec", "New Brunswick", "Nova Scotia", "Prince Edward Island", "Newfoundland and Labrador"];

    var optionOutput="";

    for(subIndex=0;subIndex<provArray.length;subIndex++){
    optionOutput+="<option name='subject" + subIndex + "' value='" + provArray[subIndex] + "'>" + provArray[subIndex] + "</option>";
    }
        document.getElementById("cboProv").innerHTML=optionOutput;
}
function validateForm(){
    if(document.forms[0].elements[0].selectedIndex==0){
        alert("Please select a Province");
        document.forms[0].elements[0].style.backgroundColor='pink';
        document.forms[0].elements[0].focus();
    return false;
    }
    if(document.forms[0].elements[1].value==""){
        alert("Please enter your name");
        document.forms[0].elements[1].style.backgroundColor='pink';
        document.forms[0].elements[1].focus();
    return false;
    }
    if(document.forms[0].elements[2].value==""){
        alert("Please enter your email");
        document.forms[0].elements[2].style.backgroundColor='pink';
        document.forms[0].elements[2].focus();
    return false;
    }


document.forms[0].submit();
}